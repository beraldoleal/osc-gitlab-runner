# OpenShift Sandboxed Container Gitlab Runner experiment.

Basic experiment for deploying a Gitlab runner on a OSC cluster.

## Requirements:

1. Have a Openshift cluster up and running
2. Install and configure OpenShift Sandbox Containers Operator

## Configuring the token

    $ sed "s/GITLAB_RUNNER_REGISTRATION_TOKEN/YOUR_TOKEN/g" -i charts/values.yaml

Replace `YOUR_TOKEN` with the Specific Runner registration token. You can find
this information at `Settings` -> `CI/CD` -> `Runners`, of your project.

## Installing the gitlab-runner

### Using the Operator

TBD

### Using helm/charts

#### Install helm

    $ sudo curl -L https://mirror.openshift.com/pub/openshift-v4/clients/helm/3.5.0/helm-linux-amd64 -o /usr/local/bin/helm
    $ chmod +x /usr/local/bin/helm

Or use your preferred method here.

#### Install the chart

    $ helm install --generate-name -f charts/values.yaml gitlab/gitlab-runner

At this point you should see a pod running and ready to receive jobs.

    $ oc get pods | grep gitlab-runner
    gitlab-runner-1657027588-7d8cdf8867-b7fqt   1/1       Running   0          17m

Also, double-check if the runner is registered at Runners page in your project.

## Trigger the pipeline

Send a MR or trigger the pipeline manually and you should be able to see the
Job running inside your kata pod.
